#
# Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Module providing base classes to EPP command responses."""
import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from datetime import datetime
from typing import Any, ClassVar, Generic, List, Mapping, Optional, Sequence, Type, TypeVar, Union, cast

from dateutil.parser import parse as parse_datetime
from dateutil.relativedelta import relativedelta
from lxml.etree import Element, QName, XMLSchema

from epplib.constants import NAMESPACE
from epplib.exceptions import ParsingError
from epplib.models import ExtractModelMixin, Statement
from epplib.responses.extensions import EXTENSIONS, ResponseExtension
from epplib.responses.poll_messages import POLL_MESSAGE_TYPES, PollMessage, RawPollMessage
from epplib.utils import (
    find_child,
    find_children,
    get_attribute,
    get_child_name,
    optional,
    parse_duration,
    safe_parse,
    text,
    texts,
)

LOGGER = logging.getLogger(__name__)

T = TypeVar('T', bound=ExtractModelMixin)
ResponseT = TypeVar('ResponseT', bound='Response')
ResultT = TypeVar('ResultT', bound='Result')

GreetingPayload = Mapping[str, Union[None, Sequence[str], Sequence[Statement], datetime, relativedelta, str]]


class Response(ABC):
    """Base class for responses to EPP commands.

    Attributes:
        tag: Expected tag enclosing the response payload.
    """

    _payload_tag: ClassVar[QName]

    # Concrete Responses are supposed to be dataclasses. ABC can not be a dataclass. We need to specify init for typing.
    def __init__(self, *args: Any, **kwargs: Any) -> None:  # noqa: B027
        pass  # pragma: no cover

    @classmethod
    def parse(cls: Type[ResponseT], raw_response: bytes, schema: Optional[XMLSchema] = None) -> ResponseT:
        """Parse the xml response into the dataclass.

        Args:
            raw_response: The raw XML response which will be parsed into the Response object.
            schema: A XML schema used to validate the parsed Response. No validation is done if schema is None.

        Returns:
            Dataclass representing the EPP response.

        Raises:
            ValueError: If the root tag is not "epp" or if the tag representing the type of the response is not the one
                expected by the parser.
            ParsingError: If parsing fails for whatever reason. ParsingError wraps the original exceptions and adds the
                raw data received from the server to ease the debugging.
        """
        root = safe_parse(raw_response)

        if schema is not None:
            schema.assertValid(root)

        if root.tag != QName(NAMESPACE.EPP, 'epp'):
            raise ValueError('Root element has to be "epp". Found: {}'.format(root.tag))

        payload = root[0]
        if payload.tag != cls._payload_tag:
            raise ValueError('Expected {} tag. Found {} instead.'.format(cls._payload_tag, payload.tag))
        try:
            data = cls._extract_payload(payload)
        except Exception as exception:
            raise ParsingError(raw_response=raw_response) from exception

        return cls(**data)

    @classmethod
    @abstractmethod
    def _extract_payload(cls, element: Element) -> Mapping[str, Any]:
        """Extract the actual information from the response.

        Args:
            element: Child element of the epp element.
        """


@dataclass
class RawResponse(Response):
    """EPP Response to a `SendRawXML` command."""

    _payload_tag: ClassVar = None

    xml_data: bytes

    @classmethod
    def parse(cls: Type[ResponseT], raw_response: bytes, schema: Optional[XMLSchema] = None) -> ResponseT:
        """Return the response with raw data."""
        return cls(raw_response)

    @classmethod
    def _extract_payload(cls, element: Element) -> Mapping[str, Any]:  # pragma: no cover
        """Do nothing, as we can't know the data format."""
        raise AttributeError("Cannot extract payload of a raw XML response.")


@dataclass
class Greeting(Response):
    """EPP Greeting representation.

    Attributes:
        sv_id: Content of the epp/greeting/svID element.
        sv_date: Content of the epp/greeting/svDate element.
        versions: Content of the epp/greeting/svcMenu/version element.
        langs: Content of the epp/greeting/svcMenu/lang element.
        obj_uris: Content of the epp/greeting/svcMenu/objURI element.
        ext_uris: Content of the epp/greeting/svcMenu/svcExtension/extURI element.
        access: Content of the epp/greeting/dcp/access element.
        statements: Content of the epp/greeting/statement element.
        expiry: Content of the epp/greeting/expiry element.
    """

    _namespace: ClassVar[str] = NAMESPACE.EPP
    _payload_tag: ClassVar = QName(_namespace, 'greeting')

    sv_id: str
    sv_date: str
    versions: List[str]
    langs: List[str]
    obj_uris: List[str]
    ext_uris: List[str]
    access: str
    statements: List[Statement]
    expiry: Optional[str]

    @classmethod
    def parse(cls, raw_response: bytes, schema: Optional[XMLSchema] = None) -> 'Greeting':
        """Parse the xml response into the Greeting dataclass.

        Args:
            raw_response: The raw XML response which will be parsed into the Response object.
            schema: A XML schema used to validate the parsed Response. No validation is done if schema is None.
        """
        return super().parse(raw_response, schema)

    @classmethod
    def _extract_payload(cls, element: Element) -> GreetingPayload:
        """Extract the actual information from the response.

        Args:
            element: Child element of the epp element.
        """
        data = {
            'sv_id': text(find_child(element, QName(cls._namespace, 'svID')), strict=True),
            'sv_date': text(find_child(element, QName(cls._namespace, 'svDate')), strict=True),

            'versions': texts(find_children(element, [QName(cls._namespace, 'svcMenu'),
                                                      QName(cls._namespace, 'version')])),
            'langs': texts(find_children(element, [QName(cls._namespace, 'svcMenu'),
                                                   QName(cls._namespace, 'lang')])),
            'obj_uris': texts(find_children(element, [QName(cls._namespace, 'svcMenu'),
                                                      QName(cls._namespace, 'objURI')])),

            'ext_uris': texts(find_children(
                element, [QName(cls._namespace, 'svcMenu'), QName(cls._namespace, 'svcExtension'),
                          QName(cls._namespace, 'extURI')])),

            'access': get_child_name(find_child(element, [QName(cls._namespace, 'dcp'),
                                                          QName(cls._namespace, 'access')])),

            'statements': [Statement.extract(item) for item in find_children(
                element, [QName(cls._namespace, 'dcp'), QName(cls._namespace, 'statement')])],
            'expiry': cls._extract_expiry(find_child(element, [QName(cls._namespace, 'dcp'),
                                                               QName(cls._namespace, 'expiry')]))
        }

        return data

    @classmethod
    def _extract_expiry(cls, element: Element) -> Union[None, datetime, relativedelta]:
        """Extract the expiry part of Greeting.

        Result depends on whether the expiry is relative or absolute. Absolute expiry is returned as datetime whereas
        relative expiry is returned as timedelta.

        Args:
            element: Expiry element of Greeting.

        Returns:
            Extracted expiry expressed as either a point in time or duration until the expiry.

        Raises:
            ParsingError: If parsing of the expiry date fails.
            ValueError: If expiry is found but it does not contain "absolute" or "relative" subelement.
        """
        if element is None:
            return None

        tag = element[0].tag
        text = element[0].text

        if tag == QName(NAMESPACE.EPP, 'absolute'):
            try:
                return parse_datetime(text)
            except ValueError as exception:
                raise ParsingError('Could not parse "{}" as absolute expiry.'.format(text)) from exception
        elif tag == QName(NAMESPACE.EPP, 'relative'):
            try:
                return parse_duration(text)
            except ValueError as exception:
                raise ParsingError('Could not parse "{}" as relative expiry.'.format(text)) from exception
        else:
            raise ValueError('Expected expiry specification. Found "{}" instead.'.format(tag))


@dataclass
class MsgQ:
    """Dataclass to represent EPP msgQ element.

    Attributes:
        count: Content of the count attribute of epp/response/msgQ tag.
        id: Content of the id attribute of epp/response/msgQ tag.
        q_date: Content of the epp/response/msgQ/qDate tag.
        msg: Content of the epp/response/msgQ/msg tag.
    """

    _namespace: ClassVar[str] = NAMESPACE.EPP

    count: Optional[int]
    id: Optional[str]
    q_date: Optional[datetime]
    msg: Optional[PollMessage]

    @classmethod
    def extract(cls, element: Element) -> 'MsgQ':
        """Extract MsgQ from the element."""
        count = optional(int, get_attribute(element, 'count'))
        id = get_attribute(element, 'id')
        q_date = optional(parse_datetime, text(find_child(element, QName(cls._namespace, 'qDate'))))
        msg = cls._extract_message(find_child(element, QName(cls._namespace, 'msg')))
        return cls(count=count, id=id, q_date=q_date, msg=msg)

    @classmethod
    def _extract_message(cls, element: Optional[Element]) -> Optional[PollMessage]:
        if (element is None) or len(element) < 1:
            return None
        else:
            message_element = element[0]
            message_class = POLL_MESSAGE_TYPES.get(message_element.tag, RawPollMessage)
            return message_class.extract(message_element)


@dataclass
class ExtValue:
    """Dataclass to represent EPP extValue element.

    Attributes:
        value: Content of the epp/response/result/extValue/value tag.
        reason: Content of the epp/response/result/extValue/reason tag.
    """

    _namespace: ClassVar[str] = NAMESPACE.EPP

    value: Optional[Element]
    reason: str

    @classmethod
    def extract(cls, element: Element) -> 'ExtValue':
        """Extract ExtValue from the element."""
        value = find_child(element, QName(cls._namespace, 'value'))
        reason = text(find_child(element, QName(cls._namespace, 'reason')), strict=True)
        return cls(value=value[0] if (value is not None and len(value)) else None, reason=reason)


@dataclass
class Result(Response, Generic[T]):
    """EPP Result representation.

    Attributes:
        code: Code attribute of the epp/response/result element.
        msg: Content of the epp/response/result/msg element.
        res_data: Content of the epp/response/result/resData element.
        cl_tr_id: Content of the epp/response/trID/clTRID element.
        sv_tr_id: Content of the epp/response/trID/svTRID element.
        values: Content of the epp/response/result/value element.
        ext_values: Content of the epp/response/result/extValue element.
        extensions: Content of the epp/response/extension element.
        msg_q: Content of the epp/response/msgQ element.
    """

    _epp_namespace: ClassVar[str] = NAMESPACE.EPP
    _payload_tag: ClassVar = QName(_epp_namespace, 'response')
    _res_data_class: ClassVar[Optional[Type[ExtractModelMixin]]] = None
    _res_data_path: ClassVar[Optional[str]] = None

    code: int
    msg: str
    res_data: Sequence[T]
    cl_tr_id: Optional[str]
    sv_tr_id: str
    values: Sequence[Element] = field(default_factory=list)
    ext_values: Sequence[ExtValue] = field(default_factory=list)
    extensions: Sequence[ResponseExtension] = field(default_factory=list)
    msg_q: Optional[MsgQ] = None

    @classmethod
    def parse(cls: Type[ResultT], raw_response: bytes, schema: Optional[XMLSchema] = None) -> ResultT:
        """Parse the xml response into the Result dataclass.

        Args:
            raw_response: The raw XML response which will be parsed into the Response object.
            schema: A XML schema used to validate the parsed Response. No validation is done if schema is None.
        """
        return super().parse(raw_response, schema)

    @classmethod
    def _extract_payload(cls, element: Element) -> Mapping[str, Any]:
        """Extract the actual information from the response.

        Args:
            element: Child element of the epp element.
        """
        payload_data = {
            'code': optional(int, get_attribute(find_child(element, QName(cls._epp_namespace, 'result')), 'code')),
            'msg': text(find_child(element, [QName(cls._epp_namespace, 'result'),
                                             QName(cls._epp_namespace, 'msg')]), strict=True),
            'values': [x[0] for x in find_children(
                element, [QName(cls._epp_namespace, 'result'), QName(cls._epp_namespace, 'value')])
                if x is not None and len(x)],
            'ext_values': cls._extract_ext_values(
                find_children(element, [QName(cls._epp_namespace, 'result'), QName(cls._epp_namespace, 'extValue')])),
            'res_data': cls._extract_data(find_child(element, QName(cls._epp_namespace, 'resData'))),
            'cl_tr_id': text(find_child(element, [QName(cls._epp_namespace, 'trID'),
                                                  QName(cls._epp_namespace, 'clTRID')])),
            'sv_tr_id': text(find_child(element, [QName(cls._epp_namespace, 'trID'),
                                                  QName(cls._epp_namespace, 'svTRID')]), strict=True),
            'extensions': cls._extract_extensions(find_child(element, QName(cls._epp_namespace, 'extension'))),
            'msg_q': cls._extract_message(find_child(element, QName(cls._epp_namespace, 'msgQ'))),
        }
        return payload_data

    @classmethod
    def _extract_data(cls, element: Optional[Element]) -> Optional[Sequence[T]]:
        """Extract the content of the resData element.

        Args:
            element: resData epp element.
        """
        if (element is None) or (cls._res_data_path is None) or (cls._res_data_class is None):
            data = None
        else:
            data = []
            for item in element.findall(cls._res_data_path):
                item_data = cast(T, cls._res_data_class.extract(item))
                data.append(item_data)
        return data

    @classmethod
    def _extract_extensions(cls, element: Optional[Element]) -> Sequence[ResponseExtension]:
        data = []
        if element is not None:
            for child in element:
                extension_class = EXTENSIONS.get(child.tag, None)
                if extension_class is None:
                    LOGGER.warning('Could not find class to extract extension %s.', child.tag)
                else:
                    data.append(extension_class.extract(child))
        return data

    @classmethod
    def _extract_message(cls, element: Optional[Element]) -> Optional[MsgQ]:
        if element is None:
            return None
        else:
            return MsgQ.extract(element)

    @classmethod
    def _extract_ext_values(self, elements: List[Element]) -> List[ExtValue]:
        """Extract `extValue`s."""
        return [ExtValue.extract(el) for el in elements]
