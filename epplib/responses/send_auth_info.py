#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Module providing responses to EPP info commands."""

from dataclasses import dataclass

from epplib.constants import NAMESPACE
from epplib.models.send_auth_info import (
    SendAuthInfoContactResultData,
    SendAuthInfoDomainResultData,
    SendAuthInfoKeysetResultData,
    SendAuthInfoNssetResultData,
)
from epplib.responses.base import Result


@dataclass
class SendAuthInfoContactResult(Result[SendAuthInfoContactResultData]):
    """Represents EPP Result which responds to the SendAuthInfoContact command.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    _res_data_path = f'./{{{NAMESPACE.NIC_CONTACT}}}sendAuthInfoData'
    _res_data_class = SendAuthInfoContactResultData


@dataclass
class SendAuthInfoDomainResult(Result[SendAuthInfoDomainResultData]):
    """Represents EPP Result which responds to the sendAuthInfoDomain command.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    _res_data_path = f'./{{{NAMESPACE.NIC_DOMAIN}}}sendAuthInfoData'
    _res_data_class = SendAuthInfoDomainResultData


@dataclass
class SendAuthInfoKeysetResult(Result[SendAuthInfoKeysetResultData]):
    """Represents EPP Result which responds to the SendAuthInfoKeyset command.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    _res_data_path = f'./{{{NAMESPACE.NIC_KEYSET}}}sendAuthInfoData'
    _res_data_class = SendAuthInfoKeysetResultData


@dataclass
class SendAuthInfoNssetResult(Result[SendAuthInfoNssetResultData]):
    """Represents EPP Result which responds to the SendAuthInfoNsset command.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    _res_data_path = f'./{{{NAMESPACE.NIC_NSSET}}}sendAuthInfoData'
    _res_data_class = SendAuthInfoNssetResultData
