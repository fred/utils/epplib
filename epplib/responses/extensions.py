#
# Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Module providing EPP response extensions."""

from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import date
from typing import ClassVar, Dict, Optional, Type

from lxml.etree import Element, QName

from epplib.constants import NAMESPACE
from epplib.models import ExtraAddr
from epplib.utils import find_child, optional, parse_date, str_to_bool, text


class ResponseExtension(ABC):
    """Base class for EPP response extension.

    Attributes:
        tag: The tag of the root element of the extension.
    """

    tag: ClassVar[QName]

    @classmethod
    @abstractmethod
    def extract(cls, element: Element) -> 'ResponseExtension':
        """Extract the extension content from the element.

        Args:
            element: XML element containg the extension data.

        Returns:
            Dataclass representing the extension.
        """


@dataclass
class EnumInfoExtension(ResponseExtension):
    """Dataclass to represent CZ.NIC ENUM extension.

    Attributes:
        val_ex_date: Content of the epp/response/extension/infData/valExDate element.
        publish: Content of the epp/response/extension/infData/publish element.
        tag: The tag of the root element of the extension.
    """

    namespace: ClassVar[str] = NAMESPACE.NIC_ENUMVAL

    tag = QName(namespace, 'infData')

    val_ex_date: Optional[date]
    publish: Optional[bool]

    @classmethod
    def extract(cls, element: Element) -> 'EnumInfoExtension':
        """Extract the extension content from the element.

        Args:
            element: XML element containg the extension data.

        Returns:
            Dataclass representing the extension.
        """
        val_ex_date = optional(parse_date, text(find_child(element, QName(cls.namespace, 'valExDate'))))
        publish = optional(str_to_bool, text(find_child(element, QName(cls.namespace, 'publish'))))
        return cls(val_ex_date=val_ex_date, publish=publish)


@dataclass
class MailingAddressExtension(ResponseExtension):
    """Dataclass to represent CZ.NIC ENUM extension.

    Attributes:
        addr: Content of the epp/response/extension/infData/mailing/addr element.
    """

    namespace: ClassVar[str] = NAMESPACE.NIC_EXTRA_ADDR

    # The whole mailing address is wrapped into an infData element.
    tag = QName(namespace, 'infData')

    addr: ExtraAddr

    @classmethod
    def extract(cls, element: Element) -> 'MailingAddressExtension':
        """Extract the extension content from the element.

        Args:
            element: XML element containg the extension data.

        Returns:
            Dataclass representing the extension.
        """
        addr = ExtraAddr.extract(find_child(element, [QName(cls.namespace, 'mailing'), QName(cls.namespace, 'addr')]))
        return cls(addr=addr)


EXTENSIONS: Dict[QName, Type[ResponseExtension]] = {
    EnumInfoExtension.tag: EnumInfoExtension,
    MailingAddressExtension.tag: MailingAddressExtension,
}
