#
# Copyright (C) 2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Tests for the policy module."""
from unittest import TestCase

from epplib.models.common import Disclose, DiscloseField as DF
from epplib.policy import DataPolicy, Policy


class TestPolicy(TestCase):

    def test_disclose_policy_update(self):
        subtests = [
            # policy, input, expected

            # already correct
            (DataPolicy.ALL, Disclose(False, {DF.EMAIL, DF.ADDR}), Disclose(False, {DF.EMAIL, DF.ADDR})),
            (DataPolicy.NONE, Disclose(True, {DF.EMAIL, DF.ADDR}), Disclose(True, {DF.EMAIL, DF.ADDR})),

            # incorrect
            (DataPolicy.ALL, Disclose(True, {DF.EMAIL, DF.ADDR}),
             Disclose(False, {DF.VOICE, DF.FAX, DF.VAT, DF.IDENT, DF.NOTIFY_EMAIL})),
            (DataPolicy.NONE, Disclose(False, {DF.EMAIL, DF.ADDR}),
             Disclose(True, {DF.VOICE, DF.FAX, DF.VAT, DF.IDENT, DF.NOTIFY_EMAIL})),
        ]
        for policy, inp, expected in subtests:
            with self.subTest(policy=policy, inp=inp):
                self.assertEqual(Policy(policy).apply_disclose_policy(inp), expected)
