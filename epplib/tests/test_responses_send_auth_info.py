#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from unittest import TestCase

from epplib.models.send_auth_info import (
    SendAuthInfoContactResultData,
    SendAuthInfoDomainResultData,
    SendAuthInfoKeysetResultData,
    SendAuthInfoNssetResultData,
)
from epplib.responses import (
    SendAuthInfoContactResult,
    SendAuthInfoDomainResult,
    SendAuthInfoKeysetResult,
    SendAuthInfoNssetResult,
)
from epplib.tests.utils import BASE_DATA_PATH, SCHEMA


class TestSendAuthInfoContactResult(TestCase):
    def test_parse_minimal(self):
        xml = (BASE_DATA_PATH / 'responses/result_send_auth_info_minimal.xml').read_bytes()
        result = SendAuthInfoContactResult.parse(xml, SCHEMA)
        self.assertEqual(result.code, 1000)
        self.assertIsNone(result.res_data)

    def test_parse(self):
        xml = (BASE_DATA_PATH / 'responses/result_send_auth_info_contact.xml').read_bytes()
        result = SendAuthInfoContactResult.parse(xml, SCHEMA)
        expected = [
            SendAuthInfoContactResultData(email='a*****@r*****.*'),
        ]
        self.assertEqual(result.code, 1000)
        self.assertEqual(result.res_data, expected)

    def test_parse_error(self):
        xml = (BASE_DATA_PATH / 'responses/result_error.xml').read_bytes()
        result = SendAuthInfoContactResult.parse(xml, SCHEMA)
        self.assertEqual(result.code, 2002)


class TestSendAuthInfoDomainResult(TestCase):
    def test_parse_minimal(self):
        xml = (BASE_DATA_PATH / 'responses/result_send_auth_info_minimal.xml').read_bytes()
        result = SendAuthInfoDomainResult.parse(xml, SCHEMA)
        self.assertEqual(result.code, 1000)
        self.assertIsNone(result.res_data)

    def test_parse(self):
        xml = (BASE_DATA_PATH / 'responses/result_send_auth_info_domain.xml').read_bytes()
        result = SendAuthInfoDomainResult.parse(xml, SCHEMA)
        expected = [
            SendAuthInfoDomainResultData(email='a*****@r*****.*'),
        ]
        self.assertEqual(result.code, 1000)
        self.assertEqual(result.res_data, expected)

    def test_parse_error(self):
        xml = (BASE_DATA_PATH / 'responses/result_error.xml').read_bytes()
        result = SendAuthInfoDomainResult.parse(xml, SCHEMA)
        self.assertEqual(result.code, 2002)


class TestSendAuthInfoKeysetResult(TestCase):
    def test_parse_minimal(self):
        xml = (BASE_DATA_PATH / 'responses/result_send_auth_info_minimal.xml').read_bytes()
        result = SendAuthInfoKeysetResult.parse(xml, SCHEMA)
        self.assertEqual(result.code, 1000)
        self.assertIsNone(result.res_data)

    def test_parse(self):
        xml = (BASE_DATA_PATH / 'responses/result_send_auth_info_keyset.xml').read_bytes()
        result = SendAuthInfoKeysetResult.parse(xml, SCHEMA)
        expected = [
            SendAuthInfoKeysetResultData(email='a*****@r*****.*'),
        ]
        self.assertEqual(result.code, 1000)
        self.assertEqual(result.res_data, expected)

    def test_parse_error(self):
        xml = (BASE_DATA_PATH / 'responses/result_error.xml').read_bytes()
        result = SendAuthInfoKeysetResult.parse(xml, SCHEMA)
        self.assertEqual(result.code, 2002)


class TestSendAuthInfoNssetResult(TestCase):
    def test_parse_minimal(self):
        xml = (BASE_DATA_PATH / 'responses/result_send_auth_info_minimal.xml').read_bytes()
        result = SendAuthInfoNssetResult.parse(xml, SCHEMA)
        self.assertEqual(result.code, 1000)
        self.assertIsNone(result.res_data)

    def test_parse(self):
        xml = (BASE_DATA_PATH / 'responses/result_send_auth_info_nsset.xml').read_bytes()
        result = SendAuthInfoNssetResult.parse(xml, SCHEMA)
        expected = [
            SendAuthInfoNssetResultData(email='a*****@r*****.*'),
        ]
        self.assertEqual(result.code, 1000)
        self.assertEqual(result.res_data, expected)

    def test_parse_error(self):
        xml = (BASE_DATA_PATH / 'responses/result_error.xml').read_bytes()
        result = SendAuthInfoNssetResult.parse(xml, SCHEMA)
        self.assertEqual(result.code, 2002)
