#
# Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Module providing models for EPP info responses."""

from dataclasses import dataclass
from datetime import date, datetime
from typing import Any, ClassVar, List, Mapping, Optional, Sequence

from dateutil.parser import parse as parse_datetime
from lxml.etree import Element, QName

from epplib.constants import NAMESPACE
from epplib.models import Disclose, Dnskey, ExtractModelMixin, Ident, Ns, PostalInfo, Status
from epplib.utils import find_child, find_children, optional, parse_date, text, texts


@dataclass
class InfoResultData(ExtractModelMixin):
    """Dataclass representing queried item info in the info result.

    Attributes:
        roid: Content of the epp/response/resData/infData/roid element.
        statuses: Content of the epp/response/resData/infData/status element.
        cl_id: Content of the epp/response/resData/infData/clID element.
        cr_id: Content of the epp/response/resData/infData/crID element.
        cr_date: Content of the epp/response/resData/infData/crDate element.
        up_id: Content of the epp/response/resData/infData/upID element.
        up_date: Content of the epp/response/resData/infData/upDate element.
        tr_date: Content of the epp/response/resData/infData/trDate element.
        auth_info: Content of the epp/response/resData/infData/authInfo element.
    """

    namespace: ClassVar[str]

    roid: str
    statuses: List[Status]
    cl_id: str
    cr_id: Optional[str]
    cr_date: Optional[datetime]
    up_id: Optional[str]
    up_date: Optional[datetime]
    tr_date: Optional[datetime]
    auth_info: Optional[str]

    @classmethod
    def extract(cls, element: Element) -> 'InfoResultData':
        """Extract params for own init from the element."""
        return cls(**cls._get_params(element))

    @classmethod
    def _get_params(cls, element: Element) -> Mapping[str, Any]:
        params: Mapping[str, Any] = {
            'roid': text(find_child(element, QName(cls.namespace, 'roid')), strict=True),
            'statuses': [Status.extract(item) for item in find_children(element, QName(cls.namespace, 'status'))],
            'cl_id': text(find_child(element, QName(cls.namespace, 'clID')), strict=True),
            'cr_id': text(find_child(element, QName(cls.namespace, 'crID'))),
            'cr_date': optional(parse_datetime, text(find_child(element, QName(cls.namespace, 'crDate')))),
            'up_id': text(find_child(element, QName(cls.namespace, 'upID'))),
            'up_date': optional(parse_datetime, text(find_child(element, QName(cls.namespace, 'upDate')))),
            'tr_date': optional(parse_datetime, text(find_child(element, QName(cls.namespace, 'trDate')))),
            'auth_info': text(find_child(element, QName(cls.namespace, 'authInfo'))),
        }
        return params


@dataclass
class InfoDomainResultData(InfoResultData):
    """Dataclass representing domain info in the info domain result.

    Attributes:
        cl_id: Content of the epp/response/resData/infData/clID element.
        cr_id: Content of the epp/response/resData/infData/crID element.
        cr_date: Content of the epp/response/resData/infData/crDate element.
        up_id: Content of the epp/response/resData/infData/upID element.
        up_date: Content of the epp/response/resData/infData/upDate element.
        tr_date: Content of the epp/response/resData/infData/trDate element.
        auth_info: Content of the epp/response/resData/infData/authInfo element.
        name: Content of the epp/response/resData/infData/name element.
        registrant: Content of the epp/response/resData/infData/registrant element.
        admins: Content of the epp/response/resData/infData/admin element.
        nsset: Content of the epp/response/resData/infData/nsset element.
        keyset: Content of the epp/response/resData/infData/keyset element.
        ex_date: Content of the epp/response/resData/infData/exDate element.
    """

    namespace = NAMESPACE.NIC_DOMAIN

    name: str
    registrant: Optional[str]
    admins: List[str]
    nsset: Optional[str]
    keyset: Optional[str]
    ex_date: Optional[date]

    @classmethod
    def _get_params(cls, element: Element) -> Mapping[str, Any]:
        params: Mapping[str, Any] = {
            'name': text(find_child(element, QName(cls.namespace, 'name')), strict=True),
            'registrant': text(find_child(element, QName(cls.namespace, 'registrant'))),
            'admins': texts(find_children(element, QName(cls.namespace, 'admin'))),
            'nsset': text(find_child(element, QName(cls.namespace, 'nsset'))),
            'keyset': text(find_child(element, QName(cls.namespace, 'keyset'))),
            'ex_date': optional(parse_date, text(find_child(element, QName(cls.namespace, 'exDate')))),
        }
        return {**super()._get_params(element), **params}


@dataclass
class InfoContactResultData(InfoResultData):
    """Dataclass representing contact info in the info contact result.

    Attributes:
        cl_id: Content of the epp/response/resData/infData/clID element.
        cr_id: Content of the epp/response/resData/infData/crID element.
        cr_date: Content of the epp/response/resData/infData/crDate element.
        up_id: Content of the epp/response/resData/infData/upID element.
        up_date: Content of the epp/response/resData/infData/upDate element.
        tr_date: Content of the epp/response/resData/infData/trDate element.
        auth_info: Content of the epp/response/resData/infData/authInfo element.
        id: Content of the epp/response/resData/infData/id element.
        postal_info: Content of the epp/response/resData/infData/postalInfo element.
        voice: Content of the epp/response/resData/infData/voice element.
        fax: Content of the epp/response/resData/infData/fax element.
        email: Content of the epp/response/resData/infData/email element.
        disclose: Content of the epp/response/resData/infData/disclose element.
        vat: Content of the epp/response/resData/infData/vat element.
        ident: Content of the epp/response/resData/infData/ident element.
        notify_email: Content of the epp/response/resData/infData/notifyEmail element.
    """

    namespace = NAMESPACE.NIC_CONTACT

    id: str
    postal_info: PostalInfo
    voice: Optional[str]
    fax: Optional[str]
    email: Optional[str]
    disclose: Optional[Disclose]
    vat: Optional[str]
    ident: Optional[Ident]
    notify_email: Optional[str]

    @classmethod
    def _get_params(cls, element: Element) -> Mapping[str, Any]:
        params: Mapping[str, Any] = {
            'id': text(find_child(element, QName(cls.namespace, 'id')), strict=True),
            'postal_info': PostalInfo.extract(find_child(element, QName(cls.namespace, 'postalInfo'))),
            'voice': text(find_child(element, QName(cls.namespace, 'voice'))),
            'fax': text(find_child(element, QName(cls.namespace, 'fax'))),
            'email': text(find_child(element, QName(cls.namespace, 'email'))),
            'disclose': optional(Disclose.extract, find_child(element, QName(cls.namespace, 'disclose'))),
            'vat': text(find_child(element, QName(cls.namespace, 'vat'))),
            'ident': optional(Ident.extract, find_child(element, QName(cls.namespace, 'ident'))),
            'notify_email': text(find_child(element, QName(cls.namespace, 'notifyEmail'))),
        }
        return {**super()._get_params(element), **params}


@dataclass
class InfoKeysetResultData(InfoResultData):
    """Dataclass representing keyset info in the info keyset result.

    Attributes:
        cl_id: Content of the epp/response/resData/infData/clID element.
        cr_id: Content of the epp/response/resData/infData/crID element.
        cr_date: Content of the epp/response/resData/infData/crDate element.
        up_id: Content of the epp/response/resData/infData/upID element.
        up_date: Content of the epp/response/resData/infData/upDate element.
        tr_date: Content of the epp/response/resData/infData/trDate element.
        auth_info: Content of the epp/response/resData/infData/authInfo element.
        id: Content of the epp/response/resData/infData/id element.
        dnskeys: Content of the epp/response/resData/infData/dnskey elements.
        techs: Content of the epp/response/resData/infData/tech elements.
    """

    namespace = NAMESPACE.NIC_KEYSET

    id: str
    dnskeys: Sequence[Dnskey]
    techs: Sequence[str]

    @classmethod
    def _get_params(cls, element: Element) -> Mapping[str, Any]:
        params: Mapping[str, Any] = {
            'id': text(find_child(element, QName(cls.namespace, 'id')), strict=True),
            'dnskeys': [Dnskey.extract(item) for item in find_children(element, QName(cls.namespace, 'dnskey'))],
            'techs': texts(find_children(element, QName(cls.namespace, 'tech'))),
        }
        return {**super()._get_params(element), **params}


@dataclass
class InfoNssetResultData(InfoResultData):
    """Dataclass representing nsset info in the info nsset result.

    Attributes:
        cl_id: Content of the epp/response/resData/infData/clID element.
        cr_id: Content of the epp/response/resData/infData/crID element.
        cr_date: Content of the epp/response/resData/infData/crDate element.
        up_id: Content of the epp/response/resData/infData/upID element.
        up_date: Content of the epp/response/resData/infData/upDate element.
        tr_date: Content of the epp/response/resData/infData/trDate element.
        auth_info: Content of the epp/response/resData/infData/authInfo element.
        id: Content of the epp/response/resData/infData/id element.
        nss: Content of the epp/response/resData/infData/ns elements.
        techs: Content of the epp/response/resData/infData/tech elements.
        reportlevel: Content of the epp/response/resData/infData/reportlevel elements.
    """

    namespace = NAMESPACE.NIC_NSSET

    id: str
    nss: Sequence[Ns]
    techs: Sequence[str]
    reportlevel: int

    @classmethod
    def _get_params(cls, element: Element) -> Mapping[str, Any]:
        params: Mapping[str, Any] = {
            'id': text(find_child(element, QName(cls.namespace, 'id')), strict=True),
            'nss': [Ns.extract(item) for item in find_children(element, QName(cls.namespace, 'ns'))],
            'techs': texts(find_children(element, QName(cls.namespace, 'tech'))),
            'reportlevel': int(text(find_child(element, QName(cls.namespace, 'reportlevel')), strict=True)),
        }
        return {**super()._get_params(element), **params}
