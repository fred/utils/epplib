#
# Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
"""Module providing models for EPP create responses."""
from dataclasses import MISSING, dataclass
from datetime import date, datetime
from typing import ClassVar, Optional

from dateutil.parser import parse as parse_datetime
from lxml.etree import Element, QName

from epplib.constants import NAMESPACE
from epplib.models import ContactAddr, ExtractModelMixin, PostalInfo
from epplib.utils import find_child, optional, parse_date, text


@dataclass
class CreatePostalInfo(PostalInfo):
    """Dataclass to represent EPP postalInfo element in contact creation."""

    # Name and addr is required in contact create.
    # We need to explicitely set defaults to MISSING to override defaults from PostalInfo.
    name: str = MISSING  # type: ignore[assignment]
    addr: ContactAddr = MISSING  # type: ignore[assignment]


@dataclass
class CreateDomainResultData(ExtractModelMixin):
    """Dataclass representing result of domain creation.

    Attributes:
        name: Content of the epp/response/resData/creData/name element.
        cr_date: Content of the epp/response/resData/creData/crDate element.
        ex_date: Content of the epp/response/resData/creData/exDate element.
    """

    namespace: ClassVar[str] = NAMESPACE.NIC_DOMAIN

    name: str
    cr_date: datetime
    ex_date: Optional[date] = None

    @classmethod
    def extract(cls, element: Element) -> 'CreateDomainResultData':
        """Extract params for own init from the element."""
        params = (
            text(find_child(element, QName(cls.namespace, 'name')), strict=True),
            parse_datetime(text(find_child(element, QName(cls.namespace, 'crDate')), strict=True)),
            optional(parse_date, text(find_child(element, QName(cls.namespace, 'exDate')))),
        )
        return cls(*params)


@dataclass
class CreateNonDomainResultData(ExtractModelMixin):
    """Dataclass representing result of creation of object other than domain.

    Attributes:
        id: Content of the epp/response/resData/creData/id element.
        cr_date: Content of the epp/response/resData/creData/crDate element.
    """

    namespace: ClassVar[str]

    id: str
    cr_date: datetime

    @classmethod
    def extract(cls, element: Element) -> 'CreateNonDomainResultData':
        """Extract params for own init from the element."""
        params = (
            text(find_child(element, QName(cls.namespace, 'id')), strict=True),
            parse_datetime(text(find_child(element, QName(cls.namespace, 'crDate')), strict=True)),
        )
        return cls(*params)


@dataclass
class CreateContactResultData(CreateNonDomainResultData):
    """Dataclass representing result of contact creation.

    Attributes:
        id: Content of the epp/response/resData/creData/id element.
        cr_date: Content of the epp/response/resData/creData/crDate element.
    """

    namespace = NAMESPACE.NIC_CONTACT


@dataclass
class CreateNssetResultData(CreateNonDomainResultData):
    """Dataclass representing result of nsset creation.

    Attributes:
        id: Content of the epp/response/resData/creData/id element.
        cr_date: Content of the epp/response/resData/creData/crDate element.
    """

    namespace = NAMESPACE.NIC_NSSET


@dataclass
class CreateKeysetResultData(CreateNonDomainResultData):
    """Dataclass representing result of keyset creation.

    Attributes:
        id: Content of the epp/response/resData/creData/id element.
        cr_date: Content of the epp/response/resData/creData/crDate element.
    """

    namespace = NAMESPACE.NIC_KEYSET
