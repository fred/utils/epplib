#
# Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Module providing models to EPP check responses."""

from dataclasses import dataclass
from typing import ClassVar, Optional

from lxml.etree import Element, QName

from epplib.constants import NAMESPACE
from epplib.models import ExtractModelMixin
from epplib.utils import find_child, get_attribute, str_to_bool, text


@dataclass
class CheckDomainResultData(ExtractModelMixin):
    """Dataclass representing domain availability in the check domain result.

    Attributes:
        name: Content of the epp/response/resData/chkData/cd/name element.
        avail: Avail attribute of the epp/response/resData/chkData/cd/name element.
        reason: Content of the epp/response/resData/chkData/cd/reason element.
    """

    namespace: ClassVar[str] = NAMESPACE.NIC_DOMAIN

    name: str
    avail: Optional[bool]
    reason: Optional[str] = None

    @classmethod
    def extract(cls, element: Element) -> 'CheckDomainResultData':
        """Extract params for own init from the element."""
        params = (
            text(find_child(element, QName(cls.namespace, 'name')), strict=True),
            str_to_bool(get_attribute(find_child(element, QName(cls.namespace, 'name')), 'avail')),
            text(find_child(element, QName(cls.namespace, 'reason'))),
        )
        return cls(*params)


@dataclass
class CheckContactResultData(ExtractModelMixin):
    """Dataclass representing contact availability in the check contact result.

    Attributes:
        id: Content of the epp/response/resData/chkData/cd/id element.
        avail: Avail attribute of the epp/response/resData/chkData/cd/id element.
        reason: Content of the epp/response/resData/chkData/cd/reason element.
    """

    namespace: ClassVar[str] = NAMESPACE.NIC_CONTACT

    id: str
    avail: Optional[bool]
    reason: Optional[str] = None

    @classmethod
    def extract(cls, element: Element) -> 'CheckContactResultData':
        """Extract params for own init from the element."""
        params = (
            text(find_child(element, QName(cls.namespace, 'id')), strict=True),
            str_to_bool(get_attribute(find_child(element, QName(cls.namespace, 'id')), 'avail')),
            text(find_child(element, QName(cls.namespace, 'reason'))),
        )
        return cls(*params)


@dataclass
class CheckNssetResultData(ExtractModelMixin):
    """Dataclass representing nsset availability in the check nsset result.

    Attributes:
        id: Content of the epp/response/resData/chkData/cd/id element.
        avail: Avail attribute of the epp/response/resData/chkData/cd/id element.
        reason: Content of the epp/response/resData/chkData/cd/reason element.
    """

    namespace: ClassVar[str] = NAMESPACE.NIC_NSSET

    id: str
    avail: Optional[bool]
    reason: Optional[str] = None

    @classmethod
    def extract(cls, element: Element) -> 'CheckNssetResultData':
        """Extract params for own init from the element."""
        params = (
            text(find_child(element, QName(cls.namespace, 'id')), strict=True),
            str_to_bool(get_attribute(find_child(element, QName(cls.namespace, 'id')), 'avail')),
            text(find_child(element, QName(cls.namespace, 'reason'))),
        )
        return cls(*params)


@dataclass
class CheckKeysetResultData(ExtractModelMixin):
    """Dataclass representing keyset availability in the check keyset result.

    Attributes:
        id: Content of the epp/response/resData/chkData/cd/id element.
        avail: Avail attribute of the epp/response/resData/chkData/cd/id element.
        reason: Content of the epp/response/resData/chkData/cd/reason element.
    """

    namespace: ClassVar[str] = NAMESPACE.NIC_KEYSET

    id: str
    avail: Optional[bool]
    reason: Optional[str] = None

    @classmethod
    def extract(cls, element: Element) -> 'CheckKeysetResultData':
        """Extract params for own init from the element."""
        params = (
            text(find_child(element, QName(cls.namespace, 'id')), strict=True),
            str_to_bool(get_attribute(find_child(element, QName(cls.namespace, 'id')), 'avail')),
            text(find_child(element, QName(cls.namespace, 'reason'))),
        )
        return cls(*params)
