#
# Copyright (C) 2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Module providing models for EPP info responses."""

from dataclasses import dataclass
from typing import Any, ClassVar, Mapping, Optional

from lxml.etree import Element, QName

from epplib.constants import NAMESPACE
from epplib.models import ExtractModelMixin
from epplib.utils import find_child, text


@dataclass
class SendAuthInfoResultData(ExtractModelMixin):
    """Dataclass representing queried item info in the info result.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    namespace: ClassVar[str]

    email: Optional[str]

    @classmethod
    def extract(cls, element: Element) -> 'SendAuthInfoResultData':
        """Extract params for own init from the element."""
        return cls(**cls._get_params(element))

    @classmethod
    def _get_params(cls, element: Element) -> Mapping[str, Any]:
        params: Mapping[str, Optional[str]] = {
            'email': text(find_child(element, QName(cls.namespace, 'email')), strict=False),
        }
        return params


class SendAuthInfoContactResultData(SendAuthInfoResultData):
    """Dataclass representing queried item info in the info result.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    namespace = NAMESPACE.NIC_CONTACT


class SendAuthInfoDomainResultData(SendAuthInfoResultData):
    """Dataclass representing queried item info in the info result.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    namespace = NAMESPACE.NIC_DOMAIN


class SendAuthInfoKeysetResultData(SendAuthInfoResultData):
    """Dataclass representing queried item info in the info result.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    namespace = NAMESPACE.NIC_KEYSET


class SendAuthInfoNssetResultData(SendAuthInfoResultData):
    """Dataclass representing queried item info in the info result.

    Attributes:
        email: Content of the epp/response/result/email element.
    """

    namespace = NAMESPACE.NIC_NSSET
