#
# Copyright (C) 2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Epplib Policy object."""
from dataclasses import dataclass
from enum import Enum, unique

from .commands.base import Greeting
from .models.common import Disclose, DiscloseField


@unique
class DataPolicy(str, Enum):
    """An enum for the data policy."""

    ALL = "ALL"
    NONE = "NONE"


@dataclass
class Policy:
    """EPP server policy representation."""

    default_data_policy: DataPolicy

    @classmethod
    def from_greeting(cls, greeting: Greeting) -> 'Policy':
        """Get the default server's data policy."""
        data_policy = DataPolicy.ALL if greeting.access == 'all' else DataPolicy.NONE
        return cls(default_data_policy=data_policy)

    def apply_disclose_policy(self, disclose: Disclose) -> Disclose:
        """Return a new disclose object with the correct setting according to policy.

        Note: Doesn't modify the original `disclose` object.
        """
        policy = self.default_data_policy
        if (disclose.flag and policy == DataPolicy.ALL) or (not disclose.flag and policy == DataPolicy.NONE):
            return Disclose(not disclose.flag, set(DiscloseField) - disclose.fields)
        return Disclose(disclose.flag, set(disclose.fields))
