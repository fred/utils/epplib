#
# Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""Module providing various utility functions to simplify XML parsing."""

import re
from datetime import date
from typing import Callable, Iterable, List, Literal, Optional, TypeVar, Union, cast, overload

from dateutil.parser import parse as parse_datetime
from dateutil.relativedelta import relativedelta
from lxml.etree import Element, QName, XMLParser, fromstring

U = TypeVar('U')
V = TypeVar('V')

DURATION_REGEX = re.compile(
    (
        r'^(?P<sign>-)?P(?P<years>\d+Y)?(?P<months>\d+M)?(?P<days>\d+D)?'
        r'(T'
        r'(?P<hours>\d+H)?(?P<minutes>\d+M)?'
        r'((?P<seconds>\d+)(?P<microseconds>\.\d+)?S)?'
        r')?$'
    ),
    re.ASCII
)


def safe_parse(raw_xml: bytes) -> Element:
    """Wrap lxml.etree.fromstring function to make it safer against XML attacks.

    Arguments:
        raw_xml: The raw XML response which will be parsed.

    Raises:
        ValueError: If the XML document contains doctype.
    """
    parser = XMLParser(no_network=True, resolve_entities=False)
    parsed = fromstring(raw_xml, parser=parser)  # noqa: S320 - It should be safe with resolve_entities=False.

    if parsed.getroottree().docinfo.doctype:
        raise ValueError('Doctype is not allowed.')

    return parsed


def find_child(element: Element, query: Union[QName, Iterable[QName]]) -> Optional[Element]:
    """Find the first child element of `element` by path specified by names with namespaces.

    Arguments:
        element: The element from which to start the search.
        query: This argument can be either:
            the qualified name of the child element, or
            a sequence of qualified names, forming a path to the child element.
            The path is formed as follows:

                `[QName(a,b), QName(c,d)]` becomes `{a}b/{c}d`.
    """
    if isinstance(query, QName):
        query = [query]

    return element.find("/".join(str(qname) for qname in query))


def find_children(element: Element, query: Union[QName, Iterable[QName]]) -> List[Element]:
    """Find all children elements of `element` with a given name (in a given namespace).

    Arguments:
        element: the element from which to start the search.
        query: This argument can be either:
            the qualified name of the child elements, or
            a sequence of qualified names, forming a path to the child elements.
    """
    if isinstance(query, QName):
        query = [query]

    return cast(List[str], element.findall("/".join(str(qname) for qname in query)))


def get_attribute(element: Optional[Element], attrib: str) -> Optional[str]:
    """Get an attribute of an element."""
    if element is not None:
        return cast(Optional[str], element.attrib.get(attrib))
    else:
        return None


def get_child_name(element: Optional[Element]) -> Optional[str]:
    """Get the name of the first child."""
    children_names = get_children_names(element)
    return children_names[0] if children_names else None


def get_children_names(element: Optional[Element]) -> List[str]:
    """Get the names of children elements."""
    nodes = element.findall('./*') if element is not None else []
    return [QName(item.tag).localname for item in nodes]


@overload
def text(element: Optional[Element], strict: Literal[True]) -> str:
    ...


@overload
def text(element: Optional[Element], strict: bool = False) -> Optional[str]:
    ...


def text(element: Optional[Element], strict: bool = False) -> Optional[str]:
    """Get text from an element, or `None` iff element is `None`.

    If `strict` is `True`, it raises an exception instead of returning `None`.
    """
    ret = element.text or '' if element is not None else None
    if strict and ret is None:
        raise ValueError(f"Element {element} doesn't have a text value.")
    return ret


def texts(elements: Iterable[Element]) -> List[str]:
    """Get texts of all elements."""
    # the cast is here because `text` returns none only for element=`None`, which doesn't happen here.
    return cast(List[str], [text(e) for e in elements])


def optional(function: Callable[[U], V], param: Optional[U]) -> Optional[V]:
    """Return function(param) if param is not None otherwise return None."""
    return function(param) if param is not None else None


def parse_date(value: str) -> date:
    """Parse a date from string."""
    return parse_datetime(value).date()


def parse_duration(value: str) -> relativedelta:
    """Parse duration in the 'PnYnMnDTnHnMnS' form.

    Arguments:
        value: String to be parsed.

    Returns:
        Duration expressed as `relativedelta`.

    Raises:
        ValueError: If the value can not be parsed.
    """
    value = value.strip()
    match = DURATION_REGEX.fullmatch(value)
    if match:
        groups = match.groupdict()
        sign = -1 if groups.pop('sign') else 1

        seconds = groups.pop('seconds', None)
        microseconds = groups.pop('microseconds', None)

        params = {k: int(v[:-1]) for k, v in groups.items() if v is not None}
        params['seconds'] = int(seconds) if seconds is not None else 0
        params['microseconds'] = int(10**6 * float(microseconds)) if microseconds is not None else 0
        return sign * relativedelta(**params)  # type: ignore
    else:
        raise ValueError('Can not parse string "{}" as duration.'.format(value))


def str_to_bool(value: Optional[str]) -> Optional[bool]:
    """Convert str '0' or '1' to the corresponding bool value."""
    if value is None:
        return None
    elif value.lower() in ('1', 'true'):
        return True
    elif value.lower() in ('0', 'false'):
        return False
    else:
        raise ValueError('Value "{}" is not in the list of known boolean values.'.format(value))
