ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

3.1.0 (2024-04-03)
------------------

* Add auction extenstion for check domain (#157).
* Update project setup.

3.0.0 (2024-02-01)
------------------

* Add support for python 3.12.
* Make ``Addr`` fields not optional (#150).
* Add fallback empty string to address fields (#151).
* Add handling of errors with empty reason values (#152).
* Remove ``auth_info`` from ``Create*`` commands (#153).
* Add ``SendAuthinfo`` parsing (#154).
* Update project setup.

2.0.1 (2023-11-06)
------------------

* Add a cleaner way to work with `Ident` (#149).
* Fix socket closing in tests (#148).
* Update package info.

2.0.0 (2023-09-06)
------------------

* Drop Python 3.7 support.
* Split `rem` and `add` in `update` commands (#120).
* Remove `unit` from `CreateDomain` (#139).
* Add `RawPollMessage` class as default poll message (#134).
* Save `xml` of last command, response (#130).
* Improve `Client.send` type hints (#142).
* Remove `ParseXMLMixin` NAMESPACES mapping (#143).
* Turn `ParseXMLMixin` into separate helper functions (#144).
* Parse reason tags (#138).
* Remove `__future__.annotations` from `policy` (#141).
* Prevent finishing tests before fake server closed (#137).
* Fix typos in README (#140).
* Fix licensing.
* Update project setup.

1.1.1 (2023-08-01)
-------------------

* Fix license.

1.1.0 (2023-08-01)
-------------------

* Add `poll` commands. (#111).
* Add command to send raw XML. (#112).
* Add conversion from `fields` to `hidden` and `disclosed`. (#116).
* Add disclose policy transformation method. (#119).
* Allow passing custom ``tr_id`` (#109).
* Fix order of disclose flags (#110).
* Fix postal info behavior (#113).
* Fix extraction of mailing address (#114).
* Compare `sp` to `None`. (#115).
* Update project setup.

1.0.0 (2022-12-14)
-------------------

* Add support for Python 3.11.
* Set default port (#108).
* Fix annotations.

0.2.0 (2022-09-27)
-------------------

* Add ``auth_info`` to info commands (#101).
* Fix ``UpdateDomain`` command (#105).
* Expose list commands (#104).
* Better error handling of empty response (#106).
* Update EPP testing schemas (#101).
* Log raw XML and transport errors (#102).

0.1.1 (2022-07-21)
-------------------

* Fix license.

0.1.0 (2022-07-20)
-------------------

Initial version
